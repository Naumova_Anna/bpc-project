package com.bpc.bam.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExceptionMessageEnum {

    WRONG_BALANCE_LENGTH("The length of fractional part is wrong (pay attention to the currency code"),
    WRONG_BALANCE_FORMAT("Balance format is wrong (decimal digit number, decimal point or E-notation " +
            "exponential mark are allowed)"),
    SOURCE_ACCOUNT_DOES_NOT_EXIST("Source account does not exist. Try to re-write the number"),
    DESTINATION_ACCOUNT_DOES_NOT_EXIST("Destination account does not exist. Try to re-write the number"),
    DIFFERENT_CURRENCY_TYPES("These source account and destination account have different currency types"),
    INSUFFICIENT_FUNDS("Insufficient funds");

    private final String message;

}
