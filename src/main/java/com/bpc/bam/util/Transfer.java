package com.bpc.bam.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Transfer {

    @JsonProperty("source_account")
    private String sourceAccount;

    @JsonProperty("destination_account")
    private String destinationAccount;

    private BigDecimal amount;

}
