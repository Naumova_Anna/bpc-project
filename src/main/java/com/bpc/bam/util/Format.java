package com.bpc.bam.util;

import java.text.NumberFormat;
import java.util.Currency;

public class Format {

    /**
     * Метод для форматирования BigDecimal согласно указанному коду валюты.
     */
    public static String formatBalance(String currencyCode, String rowBalance) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setCurrency(Currency.getInstance(currencyCode));
        return nf.format(Double.valueOf(rowBalance));
    }

}
