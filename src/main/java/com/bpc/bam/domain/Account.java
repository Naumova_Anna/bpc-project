package com.bpc.bam.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

@Data
@Table
@Entity
public class Account {

    @Id
    @GeneratedValue
    @Column(name = "account_id")
    private UUID id;

    @Column(nullable = false, unique = true, length = 20)
    private String number;

    @Column(nullable = false, length = 3)
    private Currency currency;

    @Column(nullable = false)
    private BigDecimal balance;

}
