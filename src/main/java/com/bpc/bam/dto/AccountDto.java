package com.bpc.bam.dto;

import lombok.Data;

@Data
public class AccountDto {

    private String number;

    private String currency;

    private String balance;

}
