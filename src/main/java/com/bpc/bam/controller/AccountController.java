package com.bpc.bam.controller;

import com.bpc.bam.dto.AccountDto;
import com.bpc.bam.exception.*;
import com.bpc.bam.service.AccountService;
import com.bpc.bam.util.Transfer;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/account")
public class AccountController {

    private final AccountService service;

    /**
     * Метод для отображения всех аккаунтов.
     */
    @GetMapping("/all")
    public Iterable<AccountDto> showAllAccounts() {
        return service.showAllAccounts();
    }

    /**
     * Метод для открытия нового аккаунта.
     */
    @PostMapping("/open")
    public String openNewAccount(@RequestBody AccountDto account)
            throws BalanceLengthException, BalanceFormatException {
        return service.openNewAccount(account);
    }

    /**
     * Метод для трансфера средств между двумя аккаунтами.
     */
    @PostMapping("/transfer")
    public boolean transferBetweenAccounts(@RequestBody Transfer transfer)
            throws AccountNotFoundException, DifferentCurrencyTypesException, InsufficientFundsException {
        return service.transferBetweenAccounts(transfer);
    }

}
