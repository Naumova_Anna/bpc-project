package com.bpc.bam.mapper;

import com.bpc.bam.domain.Account;
import com.bpc.bam.dto.AccountDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.util.Currency;

@Mapper(componentModel = "spring")
public interface AccountMapper {

    @Mappings({
            @Mapping(target = "currency", source = "currency", qualifiedByName = "GetCodeFromCurrency"),
            @Mapping(target = "balance", source = "balance", qualifiedByName = "ConvertBigDecimalToFormattedString")})
    AccountDto accountToAccountDto(Account entity);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "currency", source = "currency", qualifiedByName = "GetCurrencyFromCode"),
            @Mapping(target = "balance", source = "balance", qualifiedByName = "ConvertStringToBigDecimal")})
    Account accountDtoToAccount(AccountDto dto) throws NumberFormatException;

    @Named("GetCodeFromCurrency")
    default String getCodeFromCurrency(Currency currency) {
        return currency.getCurrencyCode();
    }

    @Named("GetCurrencyFromCode")
    default Currency getCurrencyFromCode(String code) {
        return Currency.getInstance(code);
    }

    @Named("ConvertBigDecimalToFormattedString")
    default String convertBigDecimalToFormattedString(BigDecimal balance) {
        return balance.toPlainString();
    }

    @Named("ConvertStringToBigDecimal")
    default BigDecimal convertStringToBigDecimal(String balance) throws NumberFormatException {
        return new BigDecimal(balance);
    }

}
