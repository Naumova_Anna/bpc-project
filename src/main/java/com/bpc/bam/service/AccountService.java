package com.bpc.bam.service;

import com.bpc.bam.domain.Account;
import com.bpc.bam.dto.AccountDto;
import com.bpc.bam.exception.*;
import com.bpc.bam.mapper.AccountMapper;
import com.bpc.bam.repository.AccountRepository;
import com.bpc.bam.util.Transfer;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.bpc.bam.enumerated.ExceptionMessageEnum.DESTINATION_ACCOUNT_DOES_NOT_EXIST;
import static com.bpc.bam.enumerated.ExceptionMessageEnum.SOURCE_ACCOUNT_DOES_NOT_EXIST;
import static com.bpc.bam.util.Format.formatBalance;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountMapper mapper;
    private final AccountRepository repository;

    public Iterable<AccountDto> showAllAccounts() {

        List<Account> accounts = Lists.newArrayList(repository.findAll());
        List<AccountDto> accountsDto = new ArrayList<>();

        for (Account account : accounts) {
            accountsDto.add(mapper.accountToAccountDto(account));
        }

        accountsDto.forEach(accountDto -> accountDto.setBalance(formatBalance(accountDto.getCurrency(),
                accountDto.getBalance())));

        return accountsDto;
    }

    @Transactional
    public String openNewAccount(AccountDto dto)
            throws BalanceLengthException, BalanceFormatException {

        Account entity;

        try {
            entity = mapper.accountDtoToAccount(dto);
        } catch (NumberFormatException e) {
            throw new BalanceFormatException();
        }

        if (entity.getCurrency().getDefaultFractionDigits()
                != entity.getBalance().scale()) throw new BalanceLengthException();

        entity.setNumber(RandomStringUtils.randomNumeric(20));

        repository.save(entity);

        return entity.getNumber();
    }

    @Transactional
    public boolean transferBetweenAccounts(Transfer transfer)
            throws AccountNotFoundException, DifferentCurrencyTypesException, InsufficientFundsException {

        Account sourceAccount = repository.findByNumber(transfer.getSourceAccount())
                .orElseThrow(() -> new AccountNotFoundException(SOURCE_ACCOUNT_DOES_NOT_EXIST.getMessage()));
        Account destinationAccount = repository.findByNumber(transfer.getDestinationAccount())
                .orElseThrow(() -> new AccountNotFoundException(DESTINATION_ACCOUNT_DOES_NOT_EXIST.getMessage()));

        if (!sourceAccount.getCurrency().equals(destinationAccount.getCurrency()))
            throw new DifferentCurrencyTypesException();

        if ((sourceAccount.getBalance().compareTo(transfer.getAmount()) < 0))
            throw new InsufficientFundsException();

        sourceAccount.setBalance((sourceAccount.getBalance()).subtract(transfer.getAmount()));
        destinationAccount.setBalance((destinationAccount.getBalance()).add(transfer.getAmount()));

        repository.save(sourceAccount);
        repository.save(destinationAccount);

        return true;
    }

}
