package com.bpc.bam.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static com.bpc.bam.enumerated.ExceptionMessageEnum.DIFFERENT_CURRENCY_TYPES;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DifferentCurrencyTypesException extends Exception {

    public DifferentCurrencyTypesException() {
        super(DIFFERENT_CURRENCY_TYPES.getMessage());
    }

}
