package com.bpc.bam.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static com.bpc.bam.enumerated.ExceptionMessageEnum.WRONG_BALANCE_FORMAT;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BalanceFormatException extends Exception {

    public BalanceFormatException() {
        super(WRONG_BALANCE_FORMAT.getMessage());
    }

}
