package com.bpc.bam.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static com.bpc.bam.enumerated.ExceptionMessageEnum.INSUFFICIENT_FUNDS;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InsufficientFundsException extends Exception {

    public InsufficientFundsException() {
        super(INSUFFICIENT_FUNDS.getMessage());
    }

}
