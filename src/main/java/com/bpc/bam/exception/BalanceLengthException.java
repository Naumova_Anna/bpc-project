package com.bpc.bam.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static com.bpc.bam.enumerated.ExceptionMessageEnum.WRONG_BALANCE_LENGTH;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BalanceLengthException extends Exception {

    public BalanceLengthException() {
        super(WRONG_BALANCE_LENGTH.getMessage());
    }

}
