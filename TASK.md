## Summary
Create a simple web-application for managing bank accounts.

## Business domain
Each bank account has next business properties:
- account number - unique 20-digit string;
- currency - 3-letter currency code according ISO 4217 standard (e.g. EUR, USD, RUB);
- balance - number with optional fractional part. The maximum length of fractional part depends on currency (e.g. 2
  for EUR, 0 for JPY);

## Features to implement
#### List all accounts:
Just show all existing bank accounts with actual state of their properties.

#### Open new account:
User specifies currency and initial balance for new account. Account number must be generated automatically. When the
operation will be finished the account must become available for other features (showing and transfers).

#### Transfer from account to account:
User specifies from which account to which account and how much money must be transferred. Transfer is only available
for accounts with the same currency. It's impossible to transfer more money than the source account has on its balance.
When operation will be finished the balances of used accounts must be updated.

## Technical requirements
- Back-end application MUST be implemented on Java. You can use any frameworks and application servers to simplify
  your task;
- For datastore MUST be used any RDBMS (but it's preferred to use Oracle or PostgreSQL).

If you have experience with Docker and Docker Compose it would be nice if you'll prepare configuration for your app.