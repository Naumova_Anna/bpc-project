## API управления банковскими аккаунтами (backend)

[Детали задачи описаны в файле TASK.md]

Перечень доступных операций включает в себя: 
- вывод существующих аккаунтов
- открытие нового аккаунта
- трансфер средств между двумя аккаунтами

### Подготовка для запуска:

#### Шаг 1: сборка
```
gradle build -x test
```
#### Шаг 2: запуск
```
docker-compose up
```
### Локальный порт для запуска: 
8080

### Используемые технологии:
* Java + Spring Boot
* REST
* PostgreSQL
* Spring Data JPA
* MapStruct
* Docker

#### Адреса для Postman:
* GET http://localhost:8080/api/account/all
* POST http://localhost:8080/api/account/open
  
Вид запроса:
  ```
  {
    "currency": "",
    "balance": 
  }
  ```
* POST http://localhost:8080/api/account/transfer
  
Вид запроса:
  ```
  {
    "source_account": "",
    "destination_account": "",
    "amount":
  } 
  ```