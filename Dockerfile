FROM adoptopenjdk/openjdk11:alpine-jre
ADD build/libs/bam-0.0.1-SNAPSHOT.jar docbam.jar
ENTRYPOINT ["java","-jar","docbam.jar"]